# Ntrepid Coding Project



Ntrepid coding project

Create a simple IP Address Management REST API using python on top of any data store. It will consist of the ability to add IP Addresses by CIDR block and then either acquire or release IP addresses individually. Each IP address will have a status associated with it that is either “available” or “acquired”.



The REST API must support four endpoints…


                - Create IP addresses - take in a CIDR block and add all IP addresses within that block to the database with status “available”
                ` curl -X POST -d ip_address_cidr=123.124.21.21 -d mask=29 http://127.0.0.1:8000/cidr_blocks/

                - List IP addresses - return all IP addresses in the system with their current status
                ` curl http://127.0.0.1:8000/ip_addresses/

                - Acquire an IP - set the status of a certain IP to “acquired”
                ` curl -X PUT -d status=ACQUIRED -d id=11 http://127.0.0.1:8000/ip_addresses/11/

                - Release an IP - set the status of a certain IP to “available”
                ` curl -X PUT -d status=AVAILABLE -d id=11 http://127.0.0.1:8000/ip_addresses/11/



Optional: Web interface to operate the above 4 endpoints



You can deliver as a docker image, zip up this project and email back to me, or even post it on github, whatever is easier.

# Installation instructions

git clone https://gitlab.com/jasonbournemegacorp/ntrepid-coding-project.git . && docker-compose up --build