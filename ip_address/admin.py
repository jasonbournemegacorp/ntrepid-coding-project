from django.contrib import admin
from ip_address.models import IP_Address, CIDR_Block


class IPAddressAdmin(admin.ModelAdmin):
    pass

class CIDR_BlockAdmin(admin.ModelAdmin):
    pass

admin.site.register(IP_Address, IPAddressAdmin)
admin.site.register(CIDR_Block, CIDR_BlockAdmin)
