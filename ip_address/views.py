from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from ip_address.serializers import UserSerializer, GroupSerializer, IPAddressSerializer, CIDRBlockSerializer
from ip_address.models import IP_Address, CIDR_Block


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class IPAddressViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewd or edited.
    """
    queryset = IP_Address.objects.all()
    serializer_class = IPAddressSerializer


class CIDRBlockViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewd or edited.
    """
    queryset = CIDR_Block.objects.all()
    serializer_class = CIDRBlockSerializer