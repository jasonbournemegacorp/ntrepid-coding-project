from django.db import models
from enum import Enum
# Create your models here.

class Status(Enum):
    AVAILABLE = 'AVAILABLE'
    ACQUIRED = 'ACQUIRED'

    @classmethod
    def choices(cls):
        print(tuple((i.name, i.value) for i in cls))
        return tuple((i.name, i.value) for i in cls)


class CIDR_Block(models.Model):
    # mask will be 16 to 32
    # number of IP addresses that it will generate ip_address_count = 2**(32 - mask)
    mask = models.IntegerField()
    created_at = models.DateTimeField('Created At', auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    ip_address_cidr = models.GenericIPAddressField()


class IP_Address(models.Model):
    status = models.CharField(max_length=20, choices=Status.choices())
    created_at = models.DateTimeField('Created At', auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    ip_address = models.GenericIPAddressField()
    cidr_block = models.ForeignKey(CIDR_Block, on_delete=models.CASCADE)