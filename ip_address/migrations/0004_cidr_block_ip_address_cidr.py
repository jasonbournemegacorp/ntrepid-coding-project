# Generated by Django 2.2.4 on 2019-08-11 02:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ip_address', '0003_auto_20190809_2107'),
    ]

    operations = [
        migrations.AddField(
            model_name='cidr_block',
            name='ip_address_cidr',
            field=models.GenericIPAddressField(default='123.123.123.21'),
            preserve_default=False,
        ),
    ]
