from django.contrib.auth.models import User, Group
from rest_framework import serializers
from ip_address.models import IP_Address, CIDR_Block
from netaddr import IPNetwork


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class IPAddressSerializer(serializers.Serializer):
    status = serializers.CharField(max_length=20, required=False)
    ip_address = serializers.IPAddressField(required=False)
    id = serializers.IntegerField(required=False)
    class Meta:
        model = IP_Address
        fields = ['status', 'created_at', 'updated_at', 'ip_address', 'id']
    
    def update(self, instance, validated_data):
        """
        Update the IP status
        """
        ip_address = IP_Address.objects.get(id=validated_data.get('id'))
        ip_address.status = validated_data.get('status', instance.status)
        ip_address.save()
        return ip_address


class CIDRBlockSerializer(serializers.Serializer):
    mask = serializers.IntegerField(required=False)
    ip_address_cidr = serializers.IPAddressField(required=False)
    
    class Meta:
        model = CIDR_Block
        fields = ['mask', 'created_at', 'updated_at', 'ip_address_cidr', 'id']

    def create(self, request):
        mask = request.get('mask')
        ip_address = request.get('ip_address_cidr')
        ip_address_mask = ip_address + '/' + str(mask)

        cidr = CIDR_Block()
        cidr.mask = mask
        cidr.ip_address_cidr = ip_address
        cidr.save()

        ip_addresses = IPNetwork(ip_address_mask)
        for ip_address in ip_addresses:
            ip = IP_Address()
            ip.status = 'AVAILABLE'
            ip.ip_address = ip_address.words
            ip.cidr_block = cidr
            ip.save()
        
        return ip_addresses
        